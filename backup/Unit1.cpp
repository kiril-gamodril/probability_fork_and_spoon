//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------


#include "math.h"
#include "math.hpp"

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glaux.h>

//#define GPS




#ifndef GPS
        #define G_ACCELERATION 9.81
        #define BUFFSIZE 60000
        #define BUFFSIZE_div 800.0
        #define SYS_CLOCK 72
        #define NUMBER_OF_SAMPLES 8
        //#define SAMPLE_TYPE 13.5
        //#define SAMPLE_TYPE 71.5
        #define SAMPLE_TYPE 239.5
#else
        #define BUFFSIZE 4500
        #define BUFFSIZE_div 200.0
#endif

#define TYPE double





TYPE buf[BUFFSIZE];
#define SIZE_OF_MASSIV BUFFSIZE


#define Y_MAX 500
TYPE N=BUFFSIZE; // ����� ��������
TYPE arrayA[BUFFSIZE];


HGLRC ghRC;
HDC   ghDC;

HANDLE hFile;

TYPE Period_time;

TYPE massive_max;
TYPE massive_min;
TYPE massive_average;


TYPE probability[(int)BUFFSIZE_div];
TYPE probability_max;
TYPE probability_min;


TYPE gauss[BUFFSIZE];
TYPE gauss_max;
TYPE gauss_min;

TYPE dispersion;



void Draw()
{
  glClear(GL_DEPTH_BUFFER_BIT |  GL_COLOR_BUFFER_BIT);


  glBegin(GL_LINES);
        glColor3f(1,0,0);
        //glVertex2f(0,  100);
        //glVertex2f(10+SIZE_OF_MASSIV, 100);

        //glVertex2f(0,  200);
        //glVertex2f(10+SIZE_OF_MASSIV, 200);

        //glVertex2f(0,  300);
        //glVertex2f(10+SIZE_OF_MASSIV, 300);

        glVertex2f(0,  400);
        glVertex2f(10+SIZE_OF_MASSIV, 400);
  glEnd();

  ///////////////////////////////////////////////////////
  //  ���������
  ////////////////////////////////////////////////////////
  glBegin(GL_LINE_STRIP);
        glColor3f(0,0,0);
        for (int i=0; i<SIZE_OF_MASSIV; i++)
        {
                glVertex2f(i+10, ((arrayA[i]-massive_average)*(90)/(massive_max-massive_min) + 405 + (massive_average-massive_min)*(90)/(massive_max-massive_min)));
        }
  glEnd();

  glBegin(GL_LINE_STRIP);
        glColor3f(0,0,1);
        glVertex2f(10, ( 405 + (massive_average-massive_min)*(90)/(massive_max-massive_min)));
        glVertex2f(SIZE_OF_MASSIV+10, ( 405 + (massive_average-massive_min)*(90)/(massive_max-massive_min)));
  glEnd();
  ////////////////////////////////////////////

    //////////
  // �����
  ///////////

  glBegin(GL_LINE_STRIP);
        glColor3f(0,1,0);
        /*
        for (int i=0; i<BUFFSIZE; i++)
        {
                //glVertex2f(i*+10, ((gauss[i])*(390)/(gauss_max-gauss_min) + 5 + (gauss_min)*(390)/(gauss_max-gauss_min)));
        glVertex2f(i+10, ((gauss[i])*(390)/(gauss_max-gauss_min) + 5));
        }
        */
         for (int i=0; i<BUFFSIZE; i++)
        {
                //glVertex2f(i*+10, ((gauss[i])*(390)/(gauss_max-gauss_min) + 5 + (gauss_min)*(390)/(gauss_max-gauss_min)));
        glVertex2f(i+10, ((gauss[i])*(390)/(probability_max-probability_min) + 5));
        }
  glEnd();
  
  //////////
  // �����������
  //////////

  glBegin(GL_LINE_STRIP);
        glColor3f(0,0,0);
        for (int i=0; i<BUFFSIZE_div; i++)
        {
                //glVertex2f(i*(BUFFSIZE/BUFFSIZE_div)+10, ((probability[i])*(390)/(probability_max-probability_min) + 5 + (probability_min)*(390)/(probability_max-probability_min)));
                glVertex2f(i*(BUFFSIZE/BUFFSIZE_div)+10, ((probability[i])*(390)/(probability_max-probability_min) + 5 - (probability_min)*(390)/(probability_max-probability_min)));

        }
  glEnd();



    glBegin(GL_LINE_STRIP);
        glColor3f(0,0,1);
        glVertex2f(((massive_average - massive_min)/(massive_max - massive_min))*SIZE_OF_MASSIV+10, 5);
        glVertex2f(((massive_average - massive_min)/(massive_max - massive_min))*SIZE_OF_MASSIV+10, 395);
  glEnd();



  ////////////////////////////////////////
   glBegin(GL_QUADS);
        glColor3f(1,1,1);
        glVertex2f(0,0);
        glVertex2f(0,Y_MAX);
        glVertex2f(SIZE_OF_MASSIV+10,Y_MAX);
        glVertex2f(SIZE_OF_MASSIV+10,0);
  glEnd();

  SwapBuffers(ghDC);
}


BOOL bSetupPixelFormat(HDC hdc)
{
    PIXELFORMATDESCRIPTOR pfd, *ppfd;
    int pixelformat;

    ppfd = &pfd;

    ppfd->nSize = sizeof(PIXELFORMATDESCRIPTOR);
    ppfd->nVersion = 1;
    ppfd->dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
    ppfd->dwLayerMask = PFD_MAIN_PLANE;
    ppfd->iPixelType = PFD_TYPE_RGBA;
    ppfd->cColorBits = 16;
    ppfd->cDepthBits = 16;
 
    ppfd->cAccumBits = 0;
    ppfd->cStencilBits = 0;

    if ((pixelformat = ChoosePixelFormat(hdc, ppfd)) == 0)
    {
        MessageBox(NULL, "ChoosePixelFormat failed", "Error", MB_OK);
        return FALSE;
    }
    if (SetPixelFormat(hdc, pixelformat, ppfd) == FALSE)
    {
        MessageBox(NULL, "SetPixelFormat failed", "Error", MB_OK);
        return FALSE;
    }
    return TRUE;
}


void FormResize (TObject *Sender)
{
glMatrixMode( GL_PROJECTION );
glLoadIdentity();
gluOrtho2D (0, SIZE_OF_MASSIV + 20, 0, Y_MAX);
glMatrixMode( GL_MODELVIEW );
glLoadIdentity();
glViewport( 0, 0, Form1->Panel1->ClientWidth, Form1->Panel1->ClientHeight);
}



void GetGauss(TYPE mean, TYPE variation)
{
        TYPE x=massive_min;
        TYPE dx = (massive_max - massive_min)/(BUFFSIZE);

        for (int i=0; i<BUFFSIZE; i++)
        {
                gauss[i] =  (1.0/(sqrt(2*M_PI*variation)))*exp((-1)*(x-mean)*(x-mean)/(2.0*variation));
                x+=dx;
        }


        gauss_max=gauss[0];
        gauss_min=gauss[0];
        for (int k=0; k<BUFFSIZE; k++)
        {
           if (gauss_max < gauss[k])
           {
              gauss_max = gauss[k];
           }
           if (gauss_min > gauss[k])
           {
              gauss_min = gauss[k];
           }
        }

/* ��������, ������� �������
        TYPE gauss_probability_ploshad=0;

        for (int k=0; k<BUFFSIZE; k++)
        {
             //real_probability_ploshad+=(probability[k]*dx);
             gauss_probability_ploshad+=(gauss[k]*dx);
        }
*/
}


void __fastcall TForm1::OnFormCreate(TObject *Sender)
{

TColor Color1 =  Form1->Color;

float R1;
float G1;
float B1;

R1 = GetRValue(ColorToRGB(Color1));  //�������
R1/=255;
G1 = GetGValue(ColorToRGB(Color1));  //������
G1/=255;
B1 = GetBValue(ColorToRGB(Color1));  //�����
B1/=255;

   ghDC = GetDC(Form1->Panel1->Handle);
   if (!bSetupPixelFormat(ghDC))
      Close();
   ghRC = wglCreateContext(ghDC);
   wglMakeCurrent(ghDC, ghRC);

   glClearColor(R1, G1, B1, 1);

   FormResize(Sender);
 
   glEnable(GL_COLOR_MATERIAL);
   glEnable(GL_DEPTH_TEST);



	TYPE t=0;

	int  k; //����� �����(�),

        char FileName[]= "Acc_x.bin";
        //char FileName[]= "Gyro_y.bin";
        //char FileName[]= "GPS_y.bin";

        hFile = CreateFile(
                FileName,                               // ��� �����
                GENERIC_READ,                           // ����� �������
                FILE_SHARE_READ ,                       // ���������� ������
                NULL,                                   // SD (�����. ������)
                OPEN_EXISTING,                          // ��� �����������
                FILE_ATTRIBUTE_NORMAL,                  // �������� �����
                0                                       // �����.������� �����
        );

        DWORD temp;

        #ifndef GPS

        float arrayB[BUFFSIZE];
        //ReadFile(hFile, arrayA, sizeof(arrayA), &temp, NULL);
        ReadFile(hFile, arrayB, sizeof(arrayB), &temp, NULL);

        for (int k=0;k<BUFFSIZE;k++)
        {
            arrayA[k]= arrayB[k];
        }

        #else
         ReadFile(hFile, arrayA, sizeof(arrayA), &temp, NULL);
        #endif

//        Period_time = (12.5 + SAMPLE_TYPE)*6*NUMBER_OF_SAMPLES/((SYS_CLOCK/8.0)*1000000.0);
//        Form1->lbFs_1_2->Caption= AnsiString("Fs/2 = " + FloatToStr((1/Period_time)/2));


        massive_max = arrayA[0];
        massive_min = arrayA[0];
        massive_average = 0;
        for (int k=0; k<BUFFSIZE; k++)
        {
           if (massive_max < arrayA[k])
           {
              massive_max = arrayA[k];
           }
           if (massive_min > arrayA[k])
           {
              massive_min = arrayA[k];
           }
           massive_average+= (arrayA[k]/BUFFSIZE);
        }

        #ifndef GPS
        //Form1->lbMassMax->Caption = FloatToStr(massive_max);
        Form1->lbMassMax->Caption = FormatFloat("0.0000",massive_max);
        //Form1->lbMassMin->Caption = FloatToStr(massive_min);
        Form1->lbMassMin->Caption = FormatFloat("0.0000",massive_min);
        #else
        Form1->lbMassMax->Caption = FormatFloat("0.000000",massive_max);
        Form1->lbMassMin->Caption = FormatFloat("0.000000",massive_min);
        #endif


        TYPE dx = (massive_max - massive_min)/(BUFFSIZE_div);

        TYPE x = massive_min;

        for (int k=0; k<BUFFSIZE_div; k++)
        {
                for (int i=0; i<BUFFSIZE; i++)
                {
                        if  ((arrayA[i]>=x)&&(arrayA[i]<(x+dx)))
                        {
                              probability[k]++;
                        }
                }
                probability[k]/=(BUFFSIZE*dx);
                x+=dx;
        }


        probability_max = probability[0];
        probability_min = probability[0];

        TYPE real_probability_ploshad=0;


        for (int k=0; k<BUFFSIZE_div; k++)
        {
             //real_probability_ploshad+=(probability[k]*dx);
             real_probability_ploshad+=(probability[k]*dx);
        }


        for (int k=0; k<BUFFSIZE_div; k++)
        {
           if (probability_max < probability[k])
           {
              probability_max = probability[k];
           }
           if (probability_min > probability[k])
           {
              probability_min = probability[k];
           }

        }






        for (int i=0; i<BUFFSIZE; i++)
        {
                dispersion += ((arrayA[i] - massive_average)*(arrayA[i] - massive_average));
                //gauss[i]=
        }
        dispersion/=BUFFSIZE;


        GetGauss(massive_average, dispersion);


        Form1->lbMean->Caption = "�������������� ��������: " + FloatToStr(massive_average);
        Form1->lbVariation->Caption = "���������: " + FloatToStr(dispersion);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::OnTimer(TObject *Sender)
{
        //dispersion=dispersion*1.01;
        //GetGauss(massive_average, dispersion);
        Draw();
}
//---------------------------------------------------------------------------


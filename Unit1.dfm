object Form1: TForm1
  Left = 1413
  Top = 249
  Width = 868
  Height = 513
  Caption = 'Noise_Research'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = OnFormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 40
    Width = 49
    Height = 29
    Caption = #1064#1091#1084
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -24
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 16
    Top = 208
    Width = 124
    Height = 29
    Caption = #1055#1083#1086#1090#1085#1086#1089#1090#1100
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -24
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 8
    Top = 248
    Width = 149
    Height = 29
    Caption = #1074#1077#1088#1086#1103#1090#1085#1086#1089#1090#1080
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -24
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object lbMean: TLabel
    Left = 176
    Top = 424
    Width = 35
    Height = 13
    Caption = 'lbMean'
  end
  object lbVariation: TLabel
    Left = 176
    Top = 448
    Width = 49
    Height = 13
    Caption = 'lbVariation'
  end
  object lbMassMin: TLabel
    Left = 96
    Top = 88
    Width = 50
    Height = 13
    Caption = 'lbMassMin'
  end
  object lbMassMax: TLabel
    Left = 96
    Top = 16
    Width = 53
    Height = 13
    Caption = 'lbMassMax'
  end
  object Panel1: TPanel
    Left = 160
    Top = 16
    Width = 689
    Height = 393
    Caption = 'Panel1'
    TabOrder = 0
  end
  object Timer1: TTimer
    Interval = 100
    OnTimer = OnTimer
    Left = 816
    Top = 416
  end
end
